import os
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import random
import numpy as np
import ast
import colorsys

#Place this script in a directory with following file structure:
# ├── parselogs.py
# ├── dir1
# │   ├── prob1.opb.algo1.log
# │   ├── prob1.opb.algo2.log
# │   ├── prob2.opb.algo1.log
# │   ├── prob2.opb.algo2.log
# │   ├── ...
# ├── dir2
# │   ├── prob3.opb.algo1.log
# │   ├── prob3.opb.algo2.log
# │   ├── ...
# ├── ...
# where each dir only contains output logs (*.log) for each benchmark run by different algorithms
# If benchmark's file path for example was /home/user/instances/PB15/OPT-SMALLINT/sroussel/BA/prob1.opb
# then the 'prob1.opb' instance's log file should be contained in 'BA' folder as the immediate parent folder.

mapp = {}
termination_row = { 'cplex': 'file not found',
                    'presolved-cplex-presolve-off': 'file not found',
                    'naps': 'file not found',
                    'open-wbo': 'file not found',
                    'presolved-roundingsat': 'file not found',
                    'presolved-sat4j': 'file not found',
                    'presolved-toysat': 'file not found',
                    'rsat-soplex': 'file not found',
                    'cutcore': 'file not found',
                    'cutcore-no-soplex': 'file not found',
                    'presolved-basic-ihs': 'file not found',
                    'presolved-candidate001': 'file not found',
                    'presolved-candidate002': 'file not found',
                    'presolved-candidate002-soplex': 'file not found',
                    'presolved-candidate002-nosoplex': 'file not found',
                    'presolved-candidate001-noabs': 'file not found',
                    'presolved-candidate002-noabs': 'file not found',
                    'presolved-candidate003-nosoplex': 'file not found',
                    'presolved-candidate004-noabs': 'file not found',
                    'candidate005': 'file not found',
                    'candidate007': 'file not found',
                    'candidate008': 'file not found',
                    'candidate005-broken': 'file not found',
                    'candidate007-broken': 'file not found',
                    'candidate008-broken': 'file not found',
                    'candidate009': 'file not found',
                    'candidate010': 'file not found',
                    'presolved-candidate005-presolve-off': 'file not found',
                    'presolved-candidate007-presolve-off': 'file not found',
                    'cplex-my-wrapper': 'file not found',
                    'candidate-noseed': 'file not found',
                    'candidate-norc': 'file not found',
                    'candidate-opths': 'file not found',
                    'candidate-abs': 'file not found',
                    'candidate-noinitlp': 'file not found',
                    'candidate-yesinitlpnorc': 'file not found',
                    'candidate-absnoseed': 'file not found',
                    'candidate-noinitlp-presolved': 'file not found',
                    'seed-cliques': 'file not found',
                    'scippresolve': 'file not found',
                    'noshuffle': 'file not found',
                    'oneshuffle': 'file not found',
                    'allshuffles': 'file not found',
                    'fiveshuffles': 'file not found',
                    'wce': 'file not found',
                    'fiveshuffles-wce': 'file not found',
                    'allshuffles-wce': 'file not found',
                    'oneshuffle-wce': 'file not found',
                    }

termination_reason = {}

#Uncomment to read cache
#with open('cache.txt') as cache_file:
#    strr = cache_file.read().split('\n')
#    mapp = ast.literal_eval(strr[0])
#    termination_reason = ast.literal_eval(strr[1])

# Parse log files
for directory in os.listdir('.'):
    #break #Comment to parse logs instead of using cache
    if directory == 'data.py' or directory == 'find_out.txt' or directory == 'cache.txt' or directory == 'renamelogs.sh' or directory == 'parselogs.py':
        continue
    for filename in os.listdir(directory):
        if filename[-4:] == '.log':
            key = ""
            first = True
            for part in filename.split('.'):
                dot = '' if first else '.'
                key += dot + part
                first = False
                if 'opb' == part:
                    break
            key = directory + '/' + key
            if key not in termination_reason:
                termination_reason[key] = termination_row.copy()
            instance_type_name = filename.split('.')[-2]
            filename = directory + '/' + filename

            instance_type = 21
            # Determine instance type by algorithm name
            if filename.endswith('candidate008.log'):
                instance_type = 0
            elif filename.endswith('candidate-noseed.log'):
                instance_type = 1
            elif filename.endswith('candidate-norc.log'):
                instance_type = 2
            elif filename.endswith('candidate-opths.log'):
                instance_type = 3
            elif filename.endswith('candidate-abs.log'):
                instance_type = 4
            elif filename.endswith('naps.log'):
                instance_type = 5
            elif filename.endswith('open-wbo.log'):
                instance_type = 6
            elif filename.endswith('presolved-roundingsat.log'):
                instance_type = 7
            elif filename.endswith('presolved-sat4j.log'):
                instance_type = 8
            elif filename.endswith('presolved-toysat.log'):
                instance_type = 9
            elif filename.endswith('rsat-soplex.log'):
                instance_type = 10
            elif filename.endswith('cutcore.log'):
                instance_type = 11
            elif filename.endswith('cutcore-no-soplex.log'):
                instance_type = 12
            elif filename.endswith('candidate-yesinitlpnorc.log'):
                instance_type = 13
            elif filename.endswith('candidate-noinitlp.log'):
                instance_type = 14
            elif filename.endswith('candidate-absnoseed.log'):
                instance_type = 15
            elif filename.endswith('noshuffle.log'):
                instance_type = 16
            elif filename.endswith('allshuffles.log'):
                instance_type = 17

            if instance_type == 21:
                continue
            if key not in mapp:
                tmp = [3600,3600,3600,3600,2500,-1,-1,-1,-1,[],[],0,[],[],-1,[],[],-1]
                mapp[key] = []
                for i in range(21):
                    mapp[key].append(tmp[:])
            if len(mapp[key]) < 21:
                tmp = [3600,3600,3600,3600,2500,-1,-1,-1,-1,[],[],0,[],[],-1,[],[],-1]
                mapp[key].append(tmp[:])
            with open(filename) as file:
                hs_time = 0
                core_time = 0
                total_time = 0
                optimum_found = False
                unknown = False
                traceback = False
                error = False
                iterations = 0
                core_time_sum = 0
                hs_time_sum = 0
                core_time_count = 0
                hs_time_count = 0
                abstract_cores = 0
                core_sizes = []
                lbs = []
                ubs = []
                progression_times = []
                hs_times = []
                core_times = -1
                abs_core_times = []
                readUB = False
                total_time_printed = False
                was_timeout = False
                abs_set = 0
                reading_best_model = False
                best_model = []
                disjoint_cores = []
                mis_time = 0
                for line in file:
                    if 'Model improving search took ' in line:
                        mis_time += float(line.split('Model improving search took ')[1].split(' seconds')[0])
                    if 'Concert exception caught in solve()' in line and 'No solution exists' in line:
                        termination_reason[key][instance_type_name] = "iperror"
                        break
                    if 's OPTIMUM FOUND' in line:
                        optimum_found = True
                        termination_reason[key][instance_type_name] = "success"
                    if 'CPU time limit exceeded' in line:
                        was_timeout = True
                        termination_reason[key][instance_type_name] = "timeout"
                    if line.startswith('real '):
                        total_time = float(line.split()[1][:-1])
                        if total_time > 3600 and termination_reason[key][instance_type_name] == "xyz":
                            termination_reason[key][instance_type_name] = "timeout"
                    if 's UNKNOWN' in line:
                        unknown = True
                        termination_reason[key][instance_type_name] = "xyz"
                    if 'Segmentation fault' in line:
                        termination_reason[key][instance_type_name] = "segfault"
                    if 'Permission denied' in line:
                        termination_reason[key][instance_type_name] = "permission"
                    if 'Cannot allocate memory' in line:
                        termination_reason[key][instance_type_name] = "memory"
                    if 's ERROR' in line:
                        error = True
                        errors += 1
                        termination_reason[key][instance_type_name] = "error"
                    if 'CPLEX Error' in line:
                        termination_reason[key][instance_type_name] = "cplex error"

                    if 'Minimal HS took' in line:
                        hs_time = float(line.split()[5])
                        progression_time = float(line.split()[0][:-1])
                        hs_times.append((progression_time, hs_time))
                        hs_time_sum += hs_time
                        hs_time_count += 1
                    if 'c LB ' in line:
                        disjoint_cores.append(0)
                        try:
                            lbs.append(int(line.split()[4].split('c')[0]))
                        except:
                            print(key, "problem reading LB:", line)
                        readUB = True
                    if 'c UB ' in line and readUB:
                        try:
                            ubs.append(int(line.split()[4].split('c')[0]))
                        except:
                            print(key, "problem reading UB:", line)
                        readUB = False
                    if 'Adding abstraction set of size' in line:
                        abs_set = max(abs_set, int(line.split()[7]))
                    if 'c hs time' in line:
                        hs_time = float(line.split()[3][:-2])
                        mapp[key][instance_type][1] = min(3600, hs_time)
                    if 'c core time' in line:
                        core_time = float(line.split()[3][:-2])
                        mapp[key][instance_type][2] = min(3600, core_time)
                    if 'c abs core maintanence time' in line:
                        abs_time = float(line.split()[5][:-2])
                        mapp[key][instance_type][3] = min(3600, abs_time)
                    if optimum_found:
                        if line.startswith('real '):
                            total_time = float(line.split()[1][:-1])
                            mapp[key][instance_type][0] = min(3600, total_time)

                if core_time_count > 0:
                    mapp[key][instance_type][5] = core_time_sum / core_time_count
                if hs_time_count > 0:
                    mapp[key][instance_type][6] = hs_time_sum / hs_time_count
                if len(core_sizes) > 0:
                    mapp[key][instance_type][8] = np.max(core_sizes)
                mapp[key][instance_type][7] = abstract_cores
                if not optimum_found:
                    mapp[key][instance_type][0] = 3600
                mapp[key][instance_type][4] = min(10e10, len(lbs))
                mapp[key][instance_type][11] = abs_set
                mapp[key][instance_type][14] = core_times
                mapp[key][instance_type][15] = mis_time

for k,v in mapp.items():
    while len(v) < 21:
        tmp = [3600,3600,3600,3600,2500,-1,-1,-1,-1,[],[],0,[],[],-1,[],[],-1]
        mapp[k].append(tmp[:])

#Uncomment to write parsed data into cache
#if len(mapp) > 0:
#    with open('cache.txt', 'w') as cache_file:
#        cache_file.write(str(mapp) + '\n' + str(termination_reason))
#    print("Wrote cache")

#Balancing instance set:

random.seed(1234)
#random.seed(6789)
#random.seed(64092)
#random.seed(5793)
#random.seed(42)
#random.seed(821)
#random.seed(80508)
#random.seed(1994)
#random.seed(0)
#random.seed(2512)
samples = []

instances_by_family = {}
for k,v in mapp.items():
    family, instance = k.split('/')
    if family not in instances_by_family:
        instances_by_family[family] = []
    instances_by_family[family].append(k)

tenorplusnineorless = random.sample(instances_by_family['10orplus'] + instances_by_family['9orless'], 30)
samples.append(('10orplus/', [x for x in tenorplusnineorless if x.startswith('10orplus/')]))
samples.append(('9orless/', [x for x in tenorplusnineorless if x.startswith('9orless/')]))

# Benchmark families to balance if sampling 20 instances
#samples.append(('caixa/', random.sample(instances_by_family['caixa'], 20)))
#samples.append(('BioRepair/', random.sample(instances_by_family['BioRepair'], 20)))
#samples.append(('Metro/', random.sample(instances_by_family['Metro'], 20)))
#samples.append(('ShiftDesign/', random.sample(instances_by_family['ShiftDesign'], 20)))
#samples.append(('Timetabling/', random.sample(instances_by_family['Timetabling'], 20)))

samples.append(('BA/', random.sample(instances_by_family['BA'], 30)))
samples.append(('NG/', random.sample(instances_by_family['NG'], 30)))

manetsamples = random.sample(instances_by_family['Base'] + instances_by_family['IC'] + instances_by_family['Multihop'] + instances_by_family['MH+IC'], 30)
samples.append(('Base/', [x for x in manetsamples if x.startswith('Base/')]))
samples.append(('IC/', [x for x in manetsamples if x.startswith('IC/')]))
samples.append(('Multihop/', [x for x in manetsamples if x.startswith('Multihop/')]))
samples.append(('MH+IC/', [x for x in manetsamples if x.startswith('MH+IC/')]))

areasamples = random.sample(instances_by_family['areaDelay'] + instances_by_family['area_delay'] + instances_by_family['area_opers'] + instances_by_family['area_partials'], 30)
samples.append(('areaDelay/', [x for x in areasamples if x.startswith('areaDelay/')]))
samples.append(('area_delay/', [x for x in areasamples if x.startswith('area_delay/')]))
samples.append(('area_opers/', [x for x in areasamples if x.startswith('area_opers/')]))
samples.append(('area_partials/', [x for x in areasamples if x.startswith('area_partials/')]))

samples.append(('aries-da_nrp/', random.sample(instances_by_family['aries-da_nrp'], 30)))

samples.append(('dt-problems/', random.sample(instances_by_family['dt-problems'], 30)))

factorsamples = random.sample(instances_by_family['factor'] + instances_by_family['factor-mod-B'], 30)
samples.append(('factor/', [x for x in factorsamples if x.startswith('factor/')]))
samples.append(('factor-mod-B/', [x for x in factorsamples if x.startswith('factor-mod-B/')]))

samples.append(('graca/', random.sample(instances_by_family['graca'], 30)))

jxxsamples = random.sample(instances_by_family['j30opt'] + instances_by_family['j60opt'] + instances_by_family['j90opt'] + instances_by_family['j120opt'], 30)
samples.append(('j30opt/', [x for x in jxxsamples if x.startswith('j30opt/')]))
samples.append(('j60opt/', [x for x in jxxsamples if x.startswith('j60opt/')]))
samples.append(('j90opt/', [x for x in jxxsamples if x.startswith('j90opt/')]))
samples.append(('j120opt/', [x for x in jxxsamples if x.startswith('j120opt/')]))

samples.append(('lion9-single-obj/', random.sample(instances_by_family['lion9-single-obj'], 30)))

logic_samples = random.sample(instances_by_family['logic_synthesis'] + instances_by_family['logic-synthesis'], 30)
samples.append(('logic-synthesis/', [x for x in logic_samples if x.startswith('logic-synthesis/')]))
samples.append(('logic_synthesis/', [x for x in logic_samples if x.startswith('logic_synthesis/')]))

mismdssamples = random.sample(instances_by_family['mis'] + instances_by_family['mds'], 30)
samples.append(('mis/', [x for x in mismdssamples if x.startswith('mis/')]))
samples.append(('mds/', [x for x in mismdssamples if x.startswith('mds/')]))

samples.append(('market-split/', [x for x in instances_by_family['market-split'] if '-opt-' in x]))

randsamples = random.sample(instances_by_family['rand.biglist'] + instances_by_family['rand.smallist'] + instances_by_family['rand.newlist'], 30)
samples.append(('rand.biglist/', [x for x in randsamples if x.startswith('rand.biglist/')]))
samples.append(('rand.smallist/', [x for x in randsamples if x.startswith('rand.smallist/')]))
samples.append(('rand.newlist/', [x for x in randsamples if x.startswith('rand.newlist/')]))

opb_samples = [x for x in instances_by_family['opb'] if not x.startswith('opb/autocorr_bern')]
opb_samples += random.sample([x for x in instances_by_family['opb'] if x.startswith('opb/autocorr_bern')], 30)
samples.append(('opb/', opb_samples))

primes_samples = [x for x in instances_by_family['primes-dimacs-cnf'] if not x.startswith('primes-dimacs-cnf/normalized-aim') and not x.startswith('primes-dimacs-cnf/normalized-ii')]
primes_samples += random.sample([x for x in instances_by_family['primes-dimacs-cnf'] if x.startswith("primes-dimacs-cnf/normalized-aim-")], 30)
primes_samples += random.sample([x for x in instances_by_family['primes-dimacs-cnf'] if x.startswith("primes-dimacs-cnf/normalized-ii")], 30)
samples.append(('primes-dimacs-cnf', primes_samples))

neos_samples = random.sample([x for x in instances_by_family['heinz'] + instances_by_family['miplib-unpacked'] + instances_by_family['milp'] if 'neos' in x], 30)
for val in ['heinz', 'miplib-unpacked', 'milp']:
    samples.append((val, [x for x in instances_by_family[val] if 'neos' not in x] + [x for x in neos_samples if x.startswith("{}/".format(val))]))

for k in [kk for kk in mapp.keys()]:
    for s in samples:
        if k.startswith(s[0]) and k not in s[1]:
            mapp.pop(k, None)

#Generate latex table of solved instances per benchmark family
run_map = {}
time_map = {}
count_per_class = {}
total_run_map = {kk:0 for kk in termination_row.keys()}
total_time_map = {kk:0 for kk in termination_row.keys()}
total_count = 0

solverToIdx = { 'candidate008': 0,
                    'candidate-noseed': 1,
                    'candidate-norc': 2,
                    'candidate-opths': 3,
                    'candidate-abs': 4,
                    'naps': 5,
                    'open-wbo': 6,
                    'presolved-roundingsat': 7,
                    'presolved-sat4j': 8,
                    'presolved-toysat': 9,
                    'rsat-soplex': 10,
                    'cutcore': 11,
                    'cutcore-no-soplex': 12,
                    'candidate-yesinitlpnorc': 13,
                    'candidate-noinitlp': 14,
                    'candidate-absnoseed': 15,
                    'wce': 16,
                    'fiveshuffles-wce': 17,
                    'allshuffles-wce': 18,
                    'oneshuffle-wce': 19,
                    }

for k,v in mapp.items():
    directory = k.split('/')[0]

    if directory in ['10orplus', '9orless']:
        directory = '10orplus/9orless'
    if directory in ['Base', 'Multihop', 'IC', 'MH+IC', 'Large']:
        directory = 'MANETs'
    if directory in ['area_opers', 'area_delay', 'area_partials', 'areaDelay']:
        directory = 'area_*'
    if directory in ['bounded_golomb_rulers', 'golomb-rulers']:
        directory = 'golomb-rulers'
    if directory.startswith('frb'):
        directory = 'frbXX-XX-opb'
    if directory.startswith('hw'):
        directory = 'hw32/hw64/hw128'
    if directory in ['j30opt', 'j60opt', 'j90opt', 'j120opt']:
        directory = 'jXXopt'
    if directory in ['mis', 'mds']:
        directory = 'mis/mds'
    if directory in ['heinz', 'miplib-unpacked', 'milp', 'pbo-missing-instances', 'mps', 'miplib2003', 'miplib3', 'miplib']:
        directory = 'miplib/other'
    if directory.startswith('rand.'):
        directory = 'rand.*list'

    if directory == 'logic_synthesis':
        directory = 'logic-synthesis'
    if directory == 'miplib/other' and 'neos' in k:
        directory = 'miplib/neos'
    if directory == 'opb':
        if k.split('/')[1].startswith('autocorr_bern'):
            directory = 'opb/autocorr_bern'
        elif k.split('/')[1].startswith('edgecross'):
            directory = 'opb/edgecross'
        elif k.split('/')[1].startswith('faclay'):
            directory = 'opb/faclay'
        elif k.split('/')[1].startswith('graphpart'):
            directory = 'opb/graphpart'
        elif k.split('/')[1].startswith('pb'):
            directory = 'opb/pb'
        elif k.split('/')[1].startswith('sporttournament'):
            directory = 'opb/sporttournament'
        else:
            directory = 'opb/other'
    if directory == 'primes-dimacs-cnf':
        if k.split('/')[1].startswith('normalized-aim'):
            directory = 'primes-dimacs-cnf/aim'
        elif k.split('/')[1].startswith('normalized-ii'):
            directory = 'primes-dimacs-cnf/ii'
        elif k.split('/')[1].startswith('normalized-jnh'):
            directory = 'primes-dimacs-cnf/jnh'
        elif k.split('/')[1].startswith('normalized-par'):
            directory = 'primes-dimacs-cnf/par'
        else:
            directory = 'primes-dimacs-cnf/other'
    if directory not in run_map:
        run_map[directory] = {kk:0 for kk in termination_row.keys()}
        time_map[directory] = {kk:0 for kk in termination_row.keys()}
        count_per_class[directory] = set()
    count_per_class[directory].add(k)
    total_count += 1
    for solver in termination_row.keys():
        if solver not in termination_reason[k]:
            continue
        if termination_reason[k][solver] == 'success':
            run_map[directory][solver] += 1
            time_map[directory][solver] += v[solverToIdx[solver]][0]
            total_run_map[solver] += 1
            total_time_map[solver] += v[solverToIdx[solver]][0]

page = 0
while True:
    if 100*page >= len(time_map.keys()):
        break
    print("\\begin{table}[ht]")
    print("    \\tiny")
    print("    \centering")
    print("    \label{multiprogram}")
    print("    \\begin{tabular}{|c|l|r|l|r|}")
    print("        \hline")
    print("        Instance class &")
    print("        \multicolumn{2}{|c|}{PBO-IHS seed cliques} &")
    print("        \multicolumn{2}{|c|}{PBO-IHS SCIP presolve}\\\\")
    print("        \hline")
    for k in sorted(time_map.keys(), key=lambda x:x[0])[100*page:100*(page+1)]:
        strr = "        {} ({})".format(k.replace("_", "\_"), len(count_per_class[k]))
        solvers = ['roundingsat', 'rsat-soplex', 'cutcore-no-soplex', 'candidate-noinitlp']
        record = sorted(sorted(map(lambda x: (run_map[k][x], time_map[k][x]), solvers), key=lambda y: y[1]), key=lambda z: z[0], reverse=True)[0]
        for solver in solvers:
            record_solver = run_map[k][solver] == record[0] and time_map[k][solver] == record[1] and record[0] > 0
            strr += " & {} & {} ".format( \
                "\\textbf{{{}}}".format(run_map[k][solver]) if record_solver else "{}".format(run_map[k][solver]), \
                "\\textbf{{{:.0f}}}".format(time_map[k][solver]) if record_solver else "{:.0f}".format(time_map[k][solver]))
        print(strr, "\\\\")
        print("        \hline")
    print("    \end{tabular}")
    print("    \caption{}")
    print("\end{table}")
    page += 1

# Cactus plots
idxToSolver = ['candidate008', 'candidate-noseed', 'candidate-norc', 'candidate-opths', 'candidate-abs', 'naps', 'open-wbo', 'presolved-roundingsat', 'presolved-sat4j', 'presolved-toysat', 'rsat-soplex', 'cutcore', 'cutcore-no-soplex', 'candidate-yesinitlpnorc', 'candidate-noinitlp', 'candidate-absnoseed', 'wce', 'fiveshuffles-wce', 'allshuffles-wce', 'oneshuffle-wce', 'xxx']
results = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]

for k,v in mapp.items():
    for i in range(21):
        if idxToSolver[i] not in termination_reason[k] or termination_reason[k][idxToSolver[i]] != 'success':
            continue
        results[i].append(v[i][0])

for i in range(21):
    results[i] = sorted(results[i])

plt.rcParams.update({'font.size': 22})
fig=plt.figure(figsize=(8,8), num="cactus_plot_side_v2")
colors = ['y', 'b', 'r', 'g', 'm', 'y', 'g', 'c', 'm', 'b', 'r', 'k', 'b', 'c', 'k', 'gray', '#ff8888', '#ff8800', '#ff0088', '#ff4400', 'm']
markers = ['x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x']
labels = ['With pb-rc', 'No constraint seeding', 'No hs-rc', 'Only optimal hitting sets', 'With abstract cores', 'NaPS', 'Open-WBO', 'RS', 'Sat4J', '', 'RS/lp', '', 'RS/oll', 'Only initial LP rc', 'PBO-IHS', 'Abstract cores + no seeding', 'WCE', 'Include five smallest cores, WCE', 'Include all cores, WCE', 'Permutate assumptions, WCE']
for i in [14,0,2,3,4,1,16,17,18,19]:
    plt.plot(results[i], list(range(len(results[i]))), color=colors[i], marker=markers[i], linestyle='--', label=labels[i])
plt.grid(alpha=0.33)
plt.xlabel("Per-instance time limit (s)")
plt.ylabel("Number of benchmarks solved")
plt.legend(bbox_to_anchor=(1.05, 1))
#plt.legend()
plt.show()

# Render 1v1 plot legend separately

domain_markers = [('k', 'x'), (colorsys.hsv_to_rgb(0,1,1), 'x'), (colorsys.hsv_to_rgb(0,1,0.75), 'x'), (colorsys.hsv_to_rgb(1.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(1.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(2.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(2.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(3.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(3.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(4.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(4.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(5.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(5.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(6.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(6.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(7.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(7.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(8.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(8.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(9.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(9.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(10.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(10.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(11.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(11.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(12.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(12.0/15.0,1,0.75), 'x'), (colorsys.hsv_to_rgb(13.0/15.0,1,1), 'x'), (colorsys.hsv_to_rgb(13.0/15.0,1,0.75), 'x')]#, (colorsys.hsv_to_rgb(14.0/20.0,1,1), 'x'), (colorsys.hsv_to_rgb(14.0/20.0,1,0.5), 'x'), (colorsys.hsv_to_rgb(15.0/20.0,1,1), 'x'), (colorsys.hsv_to_rgb(15.0/20.0,1,0.5), 'x'), (colorsys.hsv_to_rgb(16.0/20.0,1,1), 'x'), (colorsys.hsv_to_rgb(16.0/20.0,1,0.5), 'x'), (colorsys.hsv_to_rgb(17.0/20.0,1,1), 'x'), (colorsys.hsv_to_rgb(17.0/20.0,1,0.5), 'x'), (colorsys.hsv_to_rgb(18.0/20.0,1,1), 'x'), (colorsys.hsv_to_rgb(18.0/20.0,1,0.5), 'x'), (colorsys.hsv_to_rgb(19.0/20.0,1,1), 'x'), (colorsys.hsv_to_rgb(19.0/20.0,1,0.5), 'x'), (colorsys.hsv_to_rgb(20.0/20.0,1,1), 'x'), (colorsys.hsv_to_rgb(20.0/20.0,1,0.5), 'x')]

plt.rcParams.update({'font.size': 12})
labels = ['other', '10orplus\n9orless\ncaixa\nrand.*list', 'area_*/trarea_ac', 'aries-da_nrp', 'BA/NG', 'MANETs', 'BioRepair', 'Metro', 'ShiftDesign', 'Timetabling', 'golomb-rulers', 'bsg', 'mds', 'mis', 'data', 'factor', 'fctp', 'featureSubscription', 'frbXX-XX-opt', 'graca/haplotype', 'hwXXX', 'jXXXopt', 'kullmann', 'lion9', 'logic-synthesis', 'market-split', 'miplib/unibo', 'opb', 'primes', 'x', 'x', 'x', 'x', 'x']
fig = plt.figure(figsize=(3, 5))
patches = [
    mpatches.Patch(color=color, label=label)
    for label, color in zip(labels, [x[0] for x in domain_markers])]
fig.legend(patches, labels, loc='center', frameon=False)
plt.show()

# 1v1 plots
comparisons = [(14,1), (14,3)]
file_titles = [
            "1v1_ihs_vs_noseed",
            "1v1_ihs_vs_opths",
            ]
titles = [
            "PBO-IHS with vs without constraint seeding",
            "PBO-IHS non-optimal vs optimal hitting sets",
        ]
xlabels = [
        "PBO-IHS with constraint seeding",
        "PBO-IHS non-optimal hitting sets",
        ]
ylabels = [
        "PBO-IHS without constraint seeding",
        "PBO-IHS optimal hitting sets",
        ]

for index,pair in enumerate(comparisons):
    x_axis = pair[0]
    y_axis = pair[1]
    results = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
    results2 = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
    results3 = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]

    for i in range(21):
        for j in range(len(domain_markers)):
            results[i].append([])
            results2[i].append([])
            results3[i].append([])
    count = 0
    for k,v in mapp.items():
        directory = k.split('/')[0]
        j = 0
        if directory in ['10orplus', '9orless', 'caixa', 'rand.biglist', 'rand.newlist', 'rand.smallist']:
            j = 1
        if directory in ['area_delay', 'areaDelay', 'area_opers', 'area_partials', 'trarea_ac']:
            j = 2
        if directory == 'aries-da_nrp':
            j = 3
        if directory in ['BA', 'NG']:
            j = 4
        if directory in ['Base', 'IC', 'Multihop', 'MH+IC', 'Large']:
            j = 5
        if directory == 'BioRepair':
            j = 6
        if directory == 'Metro':
            j = 7
        if directory == 'ShiftDesign':
            j = 8
        if directory == 'Timetabling':
            j = 9
        if directory in ['bounded_golomb_rulers', 'golomb-rulers']:
            j = 10
        if directory == 'bsg':
            j = 11
        if directory == 'mds':
            j = 12
        if directory == 'mis':
            j = 13
        if directory == 'data':
            j = 14
        if directory in ['factor', 'factor-mod-B']:
            j = 15
        if directory == 'fctp':
            j = 16
        if directory == 'featureSubscription':
            j = 17
        if directory in ['frb30-15-opb', 'frb35-17-opb', 'frb40-19-opb', 'frb45-21-opb', 'frb50-23-opb', 'frb53-24-opb', 'frb56-25-opb', 'frb59-26-opb']:
            j = 18
        if directory in ['graca', 'haplotype']:
            j = 19
        if directory in ['hw128', 'hw32', 'hw64']:
            j = 20

        if directory in ['j120opt', 'j30opt', 'j60opt', 'j90opt']:
            j = 21

        if directory == 'kullmann':
            j = 22
        if directory == 'lion9-single-obj':
            j = 23
        if directory in ['logic_synthesis', 'logic-synthesis']:
            j = 24
        if directory == 'market-split':
            j = 25
        if directory in ['milp', 'miplib', 'miplib2003', 'miplib3', 'heinz', 'unibo', 'miplib-unpacked']:
            j = 26
        if directory == 'opb':
            j = 27
        if directory == 'primes-dimacs-cnf':
            j = 28
        for i in range(21):
            results[i][j].append(v[i][0])
            results2[i][j].append(v[i][4] if v[i][4] != 10e10 and v[i][4] != 2500 else 0)

    # Render 1v1 plot
    fig=plt.figure(figsize=(7.5,7), num=file_titles[index])
    l = mlines.Line2D([0,3600], [0,3600])
    plt.gca().add_line(l)
    for i in range(len(domain_markers)):
        plt.scatter(results[x_axis][i], results[y_axis][i], color=domain_markers[i][0], marker=domain_markers[i][1])
    plt.grid(alpha=0.33)
    plt.xlabel("{}, solve time".format(xlabels[index]))
    plt.ylabel("{}, solve time".format(ylabels[index]))
    plt.show()
