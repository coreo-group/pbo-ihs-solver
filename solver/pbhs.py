#!/usr/bin/env python3
import sys
import cplex
import os
import signal
from time import time
from subprocess import Popen, PIPE
from tempfile import mkstemp
import math
import community as community_louvain
import networkx as nx
import ihssolver
import roundingsat
import argparse

start = 0
total_hs_time = 0
total_core_time = 0
total_minimize_time = 0
total_abs_time = 0
total_lp_time = 0
total_mis_time = 0

abs_core_graph = nx.Graph()

coeff_sum = 0

UB = float('inf')
best_model = []
abs_cores = []
countVarMap = {}

def time_print(*out):
    print("c {:8.3f}s - {}".format(time() - start, " ".join(list(map(str, out)))))

class TimeoutHandler:
    def __init__(self, rsat):
        self.rsat = rsat
    def __call__(self, signo, frame):
        if len(best_model) > 0:
            print("s SATISFIABLE")
            print("v ", end='')
            for mm in best_model:
                print("{} ".format(mm), end='')
        else:
            print("s UNKNOWN")
        log_exit(self.rsat)

def log_exit(rsat):
    print("c roundingsat stats:\nc")
    rsat.print()
    print("c")
    print("c hs time %.3fs" % total_hs_time)
    print("c core time %.3fs" % total_core_time)
    print("c min time %.3fs" % total_minimize_time)
    print("c abs core maintanence time %.3fs" % total_abs_time)
    print("c lp time %.3fs" % total_lp_time)
    print("c mis time %.3fs" % total_mis_time)
    print("c total time %.3fs" % (time() - start))
    exit(0)

def sign(x): return -1 if x < 0 else 1

def evaluate_objective(model):
    lit_set = set(model)
    cost = 0
    for var in obj_var_coef:
        if var in lit_set:
            cost += obj_var_coef[var]
    return cost

def parse_literal(l_str):
    return int(''.join(c for c in l_str if c != 'x'))

def model_cost(model):
    global UB, best_model
    cost = evaluate_objective(model)
    if (cost < UB):
        UB = cost
        time_print("UB", int(cost) - coeff_sum)
        best_model = model
    return cost

def get_core(rsat, assumptions, shuffles=0, usedcores=1, genericCore=True, knapsack=True, timeout=-1, rc={}):
    rsat.solve(assumptions, shuffles, genericCore, timeout)
    result = rsat.getResult()
    unitCores = set()
    cores = []
    if len(rc) > 0:
        heuristics = []
        for core in result[0]:
            if len(core[0]) == 1:
                varr = list(core[0].keys())[0]
                unitCores.add(varr if core[0][varr] > 0 else -varr)
                continue
            heuristic = -1
            for var in core[0].keys():
                if heuristic == -1 or abs(rc[abs(var)]) < heuristic:
                    heuristic = abs(rc[abs(var)])
            heuristics.append(heuristic)
            cores.append((heuristic, core))
        cores = [cc for cc in cores if cc[0] > -1]
        result = ([] if len(cores) == 0 else [cc[1] for cc in sorted(cores, key=lambda c: c[0], reverse=True)[:usedcores]], result[1], result[2])
    if len(cores) <= 0:
        if len(rc) > 0:
            time_print("Couldn't apply rc to choose core from shuffle")
        if genericCore and knapsack:
            cores = []
            for core in result[0]:
                if len(core[0]) == 1:
                    varr = list(core[0].keys())[0]
                    unitCores.add(varr if core[0][varr] > 0 else -varr)
                    continue
                quotients = sorted([(var, (float(coef) / float(obj_var_coef[var]))) for var,coef in core[0].items()], key=lambda q: q[1], reverse=True)
                lhs = 0
                heuristic = 0
                for q in quotients:
                    if lhs >= core[1]:
                        break
                    lhs += core[0][q[0]]
                    heuristic += obj_var_coef[q[0]]
                cores.append((heuristic, core))
            result = ([] if len(cores) == 0 else [cc[1] for cc in sorted(cores, key=lambda c: c[0], reverse=True)[:usedcores]], result[1], result[2])
        else:
            if usedcores > 1:
                cores = result[0][:]
                for core in result[0]:
                    if len(core[0]) == 1:
                        varr = list(core[0].keys())[0]
                        unitCores.add(varr if core[0][varr] > 0 else -varr)
                #Remove equal cores
                for i,c1 in enumerate(result[0]):
                    for j,c2 in enumerate(result[0]):
                        if j <= i:
                            continue
                        if sorted(c1[0].keys()) == sorted(c2[0].keys()):
                            cores[j] = []
                cores = [c for c in cores if len(c) > 0]
                cores_tmp = cores[:]
                #Remove supersets
                for i,c1 in enumerate(cores_tmp):
                    for j,c2 in enumerate(cores_tmp):
                        if i == j:
                            continue
                        if core_is_subset(sorted(c1[0].keys()), sorted(c2[0].keys())):
                            cores[j] = []
                cores = [c for c in cores if len(c) > 0]
                #Pick n number of smallest cores
                if len(cores) > usedcores:
                    cores = sorted(cores,key=lambda x: len(x))[:usedcores]
                result = (cores, result[1], result[2])
            else:
                bestCore = []
                firstsize = 0
                if len(result[0]) > 0:
                    firstsize = len(result[0][0][0])
                for core in result[0]:
                    if len(core[0]) == 1:
                        varr = list(core[0].keys())[0]
                        unitCores.add(varr if core[0][varr] > 0 else -varr)
                        continue
                    if bestCore == [] or len(core[0]) < len(bestCore[0]):
                        bestCore = core
                lastsize = firstsize if bestCore == [] else len(bestCore[0])
                result = ([] if bestCore == [] else [bestCore], result[1], result[2])
    else:
        time_print("Applied rc to choose core from shuffle")
    cores = result[0]
    for l in unitCores:
        cores.append(({abs(l): 1 if l > 0 else -1}, 1 if l > 0 else 0))
    status = ""
    if result[2] == 1:
        status = "SAT"
    elif result[2] == 2:
        status = "UNSAT"
    elif timeout <= 0: #Solver status is unknown if timeout was triggered
        print("s UNKNOWN")
        print("roundingsat exited with {}".format(result[2]))
        log_exit(rsat)
    result = (cores, result[1], status)
    if (not genericCore or not knapsack) and usedcores == 1 and shuffles > 0 and len(result[0]) > 0:
        time_print("shuffle %d to %d" % (firstsize, lastsize))
    return result

def minimize_core(rsat, core, type=0):
    if type == 0 or len(core) <= 1:
        return core
    if type == 2:
        mus = core[:]
        for i in range(len(core)-1, -1, -1):
            min_start = time()
            var = mus.pop(i)
            c,m,s = get_core(rsat, [-l for l in mus], shuffles=0, genericCore=False)
            if s == 'SAT':
                mus.insert(i, var)
                model_cost(m)
            min_end = time() - min_start
        time_print("minimize", len(core), "to", len(mus))
        return mus
    #if type == 1:
    mus = []

    lits = core[:]

    while len(lits):
        l, lits = lits[0], lits[1:]
        assumptions = [-l for l in lits + mus]
        min_start = time()
        c, m, s = get_core(rsat, assumptions, shuffles=0, genericCore=False)
        min_end = time() - min_start
        if len(c):
            # UNSAT: discard l
            # discard all literals not in core
            if len(core) < len(lits) + len(mus):
                lits = [l for l in core if l not in mus]
        else:
            # SAT: l in mus
            model_cost(m)
            mus.append(l)

    time_print("minimize", len(core), "to", len(mus))
    return mus

'''
Do required actions to implement an abstract core
Input:  ab - abstract core as list of literals
        rsats - roundingsat (PB solver) objects
        ihs - instance of CPLEX solver to encode HS constraints
        hs - minimal hitting set found in previous iteration
        countVarMap - map: (abstract core index, b variable count) -> variable name
        idx - index of ab
        nextVar - unoccupied variable name
Output: updated nextVar value, in case previous nextVar got occupied during this call
'''
def implementAbstractCore(ab, rsats, ihs, hs, countVarMap, idx, nextVar):
    countVar = nextVar
    subsetCount = 0
    overallCount = len(ab)
    # Count intersection between ab and hs
    for b in ab:
        if b in hs:
            subsetCount += 1
    # Terminate if hs completely contained in ab
    if subsetCount == overallCount:
        return nextVar
    # Nothing to be done if abstract core count already encoded
    if (idx, subsetCount) in countVarMap:
        return nextVar
    # Add two lines to PB instance to encode following: countVar <-> sum(b in ab) >= subsetCount+1
    ind = ab[:] + [countVar]
    val = [1] * overallCount + [-subsetCount - 1]
    for rsat in rsats:
        rsat.addConstraint(dict(zip(ind,val)), 0)
    val = [-1] * overallCount + [overallCount]
    for rsat in rsats:
        rsat.addConstraint(dict(zip(ind,val)), -subsetCount)
    # Add constraints to HS instance
    ihs.addVariable(countVar)
    ind = ab[:] + [countVar]
    val = [1] * overallCount + [-subsetCount - 1]
    ihs.addConstraint(dict(zip(ind,val)), 0, 0)
    ind = ab[:] + [countVar]
    val = [1] * overallCount + [-overallCount]
    ihs.addConstraint(dict(zip(ind,val)), 1, subsetCount)
    countVarMap[(idx, subsetCount)] = countVar
    # Return next unoccupied variable name
    return nextVar + 1

def core_is_subset(smaller_set, bigger_set):
    if len(smaller_set) > len(bigger_set):
        return False
    j = 0
    for i,b in enumerate(bigger_set):
        s = smaller_set[j]
        if b > s:
            return False
        if b == s:
            j += 1
        if j >= len(smaller_set):
            return True
    return False

if __name__ == "__main__":
    #generic cores refer to PB cores in the code
    #nongeneric cores refer to clausal cores in the code
    parser = argparse.ArgumentParser(description='PBO-IHS solver', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--printsolution', action='store_true',
        help='Print the optimal solution found')
    parser.add_argument('--coretype', type=int, choices=[0,1,2], default=1,
        help='0 - always non-generic cores\n1 - generic cores (default)\n2 - two parallel disjoint phases, one always extracts non-generic cores, the other extracts generic cores')
    parser.add_argument('--mistype', type=int, choices=[0,1,2,3], default=1,
        help='In model improving search (MIS), PB is called with an upper bounding constraint to find a solution\nwith a smaller cost than the best solution found so far\n0 - never do MIS\n1 - do MIS when LB has not changed in #misiters iterations (default)\n2 - do MIS when UB has not changed in #misiters iterations\n3 - do MIS when both UB and LB have not changed in #misiters iterations')
    parser.add_argument('--mistimeout', type=int, default=30, metavar='#mistimeout',
        help='Number of seconds alotted for one MIS call until it is interrupted and IHS is resumed.\nIf #mistimeout <= 0, then no timeout is set (default: 30s)')
    parser.add_argument('--misiters', type=int, choices=range(1,1001), default=5, metavar='[1-1000]',
        help='Depending on #mistype, if bound does not change in #misiters iterations, then MIS is called (default: 5)')
    parser.add_argument('--optimalhs', type=int, choices=[0,1,2], default=2,
        help='0 - always optimal hs\n1 - always non-optimal hs\n2 - always non-optimal hs except when non-optimal hs returns SAT (default)')
    parser.add_argument('--abstractshuffles', type=int, choices=range(0,1001), default=20, metavar='[0-1000]',
        help='Number of times assumption set is shuffled for each abstract core extraction call (default: 20)')
    parser.add_argument('--genericshuffles', type=int, choices=range(0,1001), default=5, metavar='[0-1000]',
        help='Number of times assumption set is shuffled for each generic core extraction call (default: 5)')
    parser.add_argument('--nongenericshuffles', type=int, choices=range(0,1001), default=20, metavar='[0-1000]',
        help='Number of times assumption set is shuffled for each nongeneric core extraction call (default: 20)')
    parser.add_argument('--usedabstractcores', type=int, choices=range(1,1001), default=1, metavar='[1-1000]',
        help='From at most #shuffles number of cores, X abstract cores are added to the hs solver (default: 1)')
    parser.add_argument('--usedgenericcores', type=int, choices=range(1,1001), default=5, metavar='[1-1000]',
        help='From at most #shuffles number of cores, X generic cores are added to the hs solver (default: 5)')
    parser.add_argument('--usednongenericcores', type=int, choices=range(1,1001), default=1, metavar='[1-1000]',
        help='From at most #shuffles number of cores, X smallest non-generic cores are added to the hs solver (default: 1)')
    parser.add_argument('--disableknapsack', action='store_true',
        help='With generic cores, instead of smallest cores, an approximating knapsack heuristic is used to pick cores.\nThis flag disables the heuristic to pick smallest cores')
    parser.add_argument('--mincore', type=int, choices=[0,1,2], default=0,
        help='Use subset minimization algorithm to minimize cores\n0 - do not minimize cores (default)\n1 - minimization algorithm 1\n2 - minimization algorithm 2')
    parser.add_argument('--nohsrc', action='store_true',
        help='Disable reduced cost fixing based on MCHS LP')
    parser.add_argument('--pbrc', action='store_true',
        help='Disable reduced cost fixing based on PBO LP')
    parser.add_argument('--nounitcorefix', action='store_true',
        help='Disable variable fixing based on unit cores')
    parser.add_argument('--noseed', action='store_true',
        help='Disable constraint seeding')
    parser.add_argument('--wce', type=int, choices=[0,1,2], default=1,
        help='0 - Disable WCE during disjoint phase\n1 - Enable WCE during disjoint phase\n2 - When possible, use reduced costs as weights for WCE')
    parser.add_argument('--alths', action='store_true',
        help='Alternative way of using a hitting set as an assumption set')
    parser.add_argument('--disablefliplits', action='store_true',
        help='Disable transforming the PBO instance that would set all objective function literals to have\nnon-negative coefficients')
    parser.add_argument('--clearlearnts', action='store_true',
        help='Clear learnt clauses from the memory on the PB solver after each disjoint phase')
    parser.add_argument('--abs', type=int, default=-1, metavar='#abs',
        help='Abstract cores are extracted after LB has not increased in #abs iterations\nDisables abstract core extraction if #abs <= 0 (default: -1)')
    parser.add_argument('--coreselectbyrc', action='store_true',
        help='Choose cores from shuffles based on which core has the biggest smallest reduced cost')

    parser.add_argument('instance', help='instance file name')
    args = parser.parse_args()

    if args.disablefliplits and args.coretype > 0 and not args.disableknapsack:
        print("Literals cannot have negative coefficients if knapsack heuristic is used")
        exit()
    if args.abs > 0 and args.coretype > 0:
        print("Abstract cores are not supported with generic cores")
        exit()
    if args.mincore > 0 and args.coretype > 0:
        print("Core minimization not supported with generic cores")
        exit()

    start = time()

    print("c PBO-IHS")
    in_file = args.instance

    if not os.path.exists(in_file):
        print("cannot open path \""+in_file+"\"")
        exit(1)

    print("c solving file: {}".format(in_file))
    for k,v in args.__dict__.items():
        if k in ["instance"]:
            continue
        print("c {}: {}".format(k,v))
    if args.coretype == 0 and not args.disableknapsack:
        print("c NOTICE: Knapsack heuristic is not applied for non-generic cores")
    print("c ---------------------------------\n")

    #
    # Read opb file and strip out objective function
    #
    rsat = roundingsat.Roundingsat()
    if args.mistype > 0:
        rsat_mis = roundingsat.Roundingsat()

    signal.signal(signal.SIGINT, TimeoutHandler(rsat))
    signal.signal(signal.SIGTERM, TimeoutHandler(rsat))
    signal.signal(signal.SIGXCPU, TimeoutHandler(rsat))
    if args.mistype > 0:
        signal.signal(signal.SIGINT, TimeoutHandler(rsat_mis))
        signal.signal(signal.SIGTERM, TimeoutHandler(rsat_mis))
        signal.signal(signal.SIGXCPU, TimeoutHandler(rsat_mis))

    lines = []
    constraints = []
    objectiveline = None
    nextVar = 0
    origMaxVar = 0
    genericcores = 0
    allcores1 = 0
    allcores2 = 0
    with open(in_file, 'r') as file:
        prev = ""
        for l in file:
            if l.startswith("*"):
                # comments/header doesn't end with ;
                lines.append(l)
                continue
            for kw in l.split():
                if kw[0] == 'x':
                    nextVar = max(nextVar, int(kw[1:]))
            if len(prev) > 0:
                # append to previous line
                l = prev + l
            if l.strip()[-1] != ";":
                # line continues
                prev = l.strip() + " "
                continue
            if l.startswith("min") or l.startswith("max"):
                objectiveline = l
            else:
                lines.append(l)
                sides = l[:-1].split("=")
                equality = sides[0][-1] != ">"
                rhs = int(sides[1][:-1])
                wrds = sides[0].split()
                if wrds[-1] == '>':
                    wrds = wrds[:-1]
                ind = list(map(lambda x: int(x[1:]), wrds[1::2]))
                coefs = list(map(int, wrds[::2]))
                constraints.append((dict(zip(ind,coefs)), rhs))
                if equality:
                    constraints.append((dict(zip(ind,[-v for v in coefs])), -rhs))
            prev = ""
    nextVar += 1
    origMaxVar = nextVar
    transformedObjectiveline = objectiveline[:-1].replace('-', '+')
    ihs = ihssolver.Solver(origMaxVar)
    ihs.addObjective(transformedObjectiveline if not args.disablefliplits else objectiveline[:-1])

    if args.pbrc:
        lp_solver = ihssolver.Solver(origMaxVar)
        lp_solver.addObjective(objectiveline[:-1])
        for c in constraints:
            lp_solver.addConstraint(c[0], 0, c[1])
        orig_LB, lp_model, orig_rc = lp_solver.solve_lp_relaxation()
        print("LP relaxation found lower bound {}".format(orig_LB))
        LB = math.ceil(orig_LB)

    #
    # Parse objective line
    #
    objectiveline = objectiveline[4:-2].strip().split()
    objvars  = [int(t[1:]) for t in objectiveline[1::2]]
    objcoefs = list(map(int, objectiveline[0::2]))
    negative_coeffs = []
    smallest_coeff = min(objcoefs)
    #If --disablefliplits is off, each literal that was flipped in objective function is replaced: l -> (1 - l)
    if not args.disablefliplits:
        for i in range(len(objvars)):
            if objcoefs[i] < 0:
                negative_coeffs.append(objvars[i])
                objcoefs[i] = abs(objcoefs[i])
                coeff_sum += objcoefs[i]
        for idx,constraint in enumerate(constraints):
            offset = constraint[1]
            newconstraint = {}
            for var,coef in constraint[0].items():
                if var in negative_coeffs:
                    offset += -coef
                    newconstraint[var] = -coef
                else:
                    newconstraint[var] = coef
            constraints[idx] = (newconstraint, offset)
        print("c PBO instance transformed, objective offset {}".format(coeff_sum))
    obj_var_coef = { var:coef for (var,coef) in zip(objvars, objcoefs) }

    rsat.init(origMaxVar)
    if args.mistype > 0:
        rsat_mis.init(origMaxVar)
    set_objvars = set(objvars)
    seededConstraints = 0
    #Seed constraints that only contain literals from objective function
    for idx, c in enumerate(constraints):
        rsat.addConstraint(c[0], c[1])
        if args.mistype > 0:
            rsat_mis.addConstraint(c[0], c[1])
        constraints[idx] = (c[0], c[1])
        if not args.noseed:
            if set(c[0].keys()).issubset(set_objvars):
                ihs.addConstraint(c[0], 0, c[1])
                seededConstraints += 1
    if not args.noseed:
        print("c {} out of {} constraints were seeded to the MCHS solver".format(seededConstraints, len(constraints)))

    varnames = [str(v) for v in objvars]

    print("c objective length", len(objcoefs))

    if not args.pbrc:
        LB = sum(c for c in objcoefs if c < 0)
    UB = sum(c for c in objcoefs if c > 0)

    #
    # UNSAT without assumptions?
    #
    core_start = time()
    c, m, s = get_core(rsat, [], shuffles=0)
    total_core_time += time() - core_start

    if s == "UNSAT":
        print("s UNSATISFIABLE")
        log_exit(rsat)

    cost = model_cost(m)
    print("c instance satisfiable, first bound", cost)

    i = 0
    LB_not_increased_count = 0
    hardened_vars = [False] * origMaxVar
    UB_tightened = True
    forceOptimalLB = (args.optimalhs == 0)
    MIS_counter = 0
    iterations_not_wcerc = 0
    while True:
        i += 1
        oldLB = LB
        prevUB = UB

        #
        #Solve MCHS
        #
        hs_start = time()
        LB, hs, isOptimal = ihs.solve(oldLB, oldLB if forceOptimalLB else UB)
        workUB = UB
        while LB >= workUB and not isOptimal:#HACK: resolve precision error from CPLEX
            workUB -= 1
            time_print("Problem, hs cost {} is more or equal to UB {}, most likely precision error. Solving again...".format(LB, workUB))
            LB, hs, isOptimal = ihs.solve(oldLB, oldLB if forceOptimalLB else workUB)
        hs_time = time() - hs_start
        time_print("Minimal HS took {} seconds".format(hs_time))
        total_hs_time += hs_time

        #Reduced cost fixing
        if not args.nohsrc:
            lp_start = time()
            LP_val,_,rc = ihs.solve_lp_relaxation()
            lp_time = time() - lp_start
            time_print("LP relaxation took {} seconds".format(lp_time))
            total_lp_time += lp_time

        rckeys = set()
        if not args.nohsrc:
            rckeys = rc.keys()
        elif args.pbrc:
            rckeys = orig_rc.keys()
        for k in rckeys:
            if not args.nohsrc:
                if hardened_vars[k-1]:
                    continue
                if rc[k] > 0.0:
                    if LP_val + rc[k] > UB + 0.01 or (abs(LP_val + rc[k] - UB) < 0.001 and best_model[k-1] < 0):
                        rsat.addConstraint(dict(zip([k],[-1])), 0)
                        if args.mistype > 0:
                            rsat_mis.addConstraint(dict(zip([k],[-1])), 0)
                        ihs.addConstraint(dict(zip([k],[-1])), 0, 0)
                        time_print("Falsifying variable {} from rc fix".format(k))
                        hardened_vars[k-1] = True
                elif rc[k] < 0.0:
                    if LP_val - rc[k] > UB + 0.01 or (abs(LP_val - rc[k] - UB) < 0.001 and best_model[k-1] > 0):
                        rsat.addConstraint(dict(zip([k],[1])), 1)
                        if args.mistype > 0:
                            rsat_mis.addConstraint(dict(zip([k],[1])), 1)
                        ihs.addConstraint(dict(zip([k],[1])), 0, 1)
                        time_print("Hardening variable {} from rc fix".format(k))
                        hardened_vars[k-1] = True
            if args.pbrc:
                if hardened_vars[k-1]:
                    continue
                if UB_tightened:
                    if orig_rc[k] > 0.0:
                        if orig_LB + orig_rc[k] > UB + 0.01 or (abs(orig_LB + orig_rc[k] - UB) < 0.001 and best_model[k-1] < 0):
                            rsat.addConstraint(dict(zip([k],[-1])), 0)
                            if args.mistype > 0:
                                rsat_mis.addConstraint(dict(zip([k],[-1])), 0)
                            ihs.addConstraint(dict(zip([k],[-1])), 0, 0)
                            time_print("Falsifying variable {} from pb fix".format(k))
                            hardened_vars[k-1] = True
                    elif orig_rc[k] < 0.0:
                        if orig_LB - orig_rc[k] > UB + 0.01 or (abs(orig_LB - orig_rc[k] - UB) < 0.001 and best_model[k-1] > 0):
                            rsat.addConstraint(dict(zip([k],[1])), 1)
                            if args.mistype > 0:
                                rsat_mis.addConstraint(dict(zip([k],[1])), 1)
                            ihs.addConstraint(dict(zip([k],[1])), 0, 1)
                            time_print("Hardening variable {} from pb fix".format(k))
                            hardened_vars[k-1] = True
        UB_tightened = False
        #Form assumption set from hs returned by MCHS solver
        if args.alths:
            hs1 = set([h for h in hs if (obj_var_coef[abs(h)] > 0 and h < 0) or (obj_var_coef[abs(h)] < 0 and h > 0)])
            hs2 = set([h for h in hs if (obj_var_coef[abs(h)] > 0 and h < 0) or (obj_var_coef[abs(h)] < 0 and h > 0)])
        else:
            hs1 = set(hs)
            hs2 = set(hs)

        if isOptimal:
            time_print("LB was optimal, updated")
            if args.mistype in [1,3] and oldLB != LB:
                MIS_counter = 0
                time_print("LB increased, resetting MIS counter")
        else:
            LB = oldLB
        time_print("LB", int(LB) - coeff_sum)
        time_print("UB", int(UB) - coeff_sum)
        forceOptimalLB = (args.optimalhs == 0)

        #UB == LB, terminate
        if (abs(UB - LB) < 1e-7): break

        assumps = set([h for h in hs])
        opt_hs = True
        disjoints = 0

        if args.wce > 0:
            tmp_obj_coef1 = { var:(coef-smallest_coeff+1 if smallest_coeff < 0 else coef) for var,coef in obj_var_coef.items() }
            tmp_obj_coef2 = { var:(coef-smallest_coeff+1 if smallest_coeff < 0 else coef) for var,coef in obj_var_coef.items() }
            tmp_obj_coef_abs = { var:(coef-smallest_coeff+1 if smallest_coeff < 0 else coef) for var,coef in obj_var_coef.items() }
        if args.wce == 2:
            tmp_rc1 = { var:abs(rc[var]) for var in objvars }
            tmp_rc2 = { var:abs(rc[var]) for var in objvars }
            tmp_rc_abs = { var:abs(rc[var]) for var in objvars }
            assumps.difference_update(set(-l for l in objvars if tmp_rc_abs[abs(l)] <= 0))
            assumps.difference_update(set( l for l in objvars if tmp_rc_abs[abs(l)] <= 0))
            hs1.difference_update(set(-l for l in objvars if tmp_rc1[abs(l)] <= 0))
            hs1.difference_update(set( l for l in objvars if tmp_rc1[abs(l)] <= 0))
            hs2.difference_update(set(-l for l in objvars if tmp_rc2[abs(l)] <= 0))
            hs2.difference_update(set( l for l in objvars if tmp_rc2[abs(l)] <= 0))
        wce_by_rc = (args.wce == 2)

        if args.abs > 0 and LB <= oldLB:
            if LB_not_increased_count < args.abs:
                LB_not_increased_count += 1
            else:
                abs_start = time()
                #Cluster variable graph
                dendo = community_louvain.generate_dendrogram(abs_core_graph, random_state=123)
                #Use second argument to control the level of the louvain cluster to extract
                prt = community_louvain.partition_at_level(dendo, 0)
                #prt = community_louvain.best_partition(abs_core_graph, random_state=123)
                partitions = []
                for k,v in prt.items():
                    while v >= len(partitions):
                        partitions.append([])
                    partitions[v].append(k)
                #Implement new abstract cores based on cluster
                for p in partitions:
                    addPartition = True
                    p = sorted(p)
                    for idx, ab in enumerate(abs_cores):
                        if core_is_subset(p, sorted(ab)):
                            addPartition = False
                            break
                        elif core_is_subset(ab, p):
                            abs_cores[idx] = p[:]
                            addPartition = False
                    if addPartition:
                        abs_cores.append(p)
                        time_print("Adding abstraction set of size {}".format(len(p)))
                #Go through abstract core catalogue and implement missing abs core count constraints
                for idx, ab in enumerate(abs_cores):
                    subsetCount = 0
                    overallCount = len(ab)
                    for b in ab:
                        if -b in assumps:
                            assumps.remove(-b)
                        if b in hs:
                            subsetCount += 1
                    nextVar = implementAbstractCore(ab, [rsat] if args.mistype == 0 else [rsat, rsat_mis], ihs, hs, countVarMap, idx, nextVar)
                    if subsetCount != overallCount:
                        if args.wce > 0:
                            tmp_obj_coef_abs[abs(countVarMap[(idx, subsetCount)])] = obj_var_coef[ab[0]]
                        assumps.add(-countVarMap[(idx, subsetCount)])
                total_abs_time += time() - abs_start
        else:
            LB_not_increased_count = 0

        if args.clearlearnts:
            learnts = rsat.clearLearnedConstraints()
            time_print("Removed", learnts, "learned constraints")

        should_extract_abs_core = (args.abs > 0) and (hs != list(assumps))
        should_extract_generic_core = args.coretype in [1,2]
        should_extract_nongnrc_core = args.coretype in [0,2]
        #Disjoint phase
        while True:
            assumptions = list(assumps)

            c = []
            c1 = []
            c2 = []
            #Extract abstract core
            #Different set of assumptions is used for extracing abstract cores than for extracting standard cores
            if should_extract_abs_core:
                core_start = time()
                c, m, s = get_core(rsat, assumptions, shuffles=args.abstractshuffles, genericCore=False, usedcores=args.usedabstractcores, rc=(rc if args.coreselectbyrc else {}))
                core_time = time() - core_start
                time_print("Abstract Core sizes {}, took {} seconds".format([len(cc[0].keys()) for cc in c], core_time))
                total_core_time += core_time
                for cc in c:
                    if len(cc[0]) == 1 and not args.nounitcorefix:
                        var,coef = list(cc[0].items())[0]
                        if abs(var) < origMaxVar and not hardened_vars[var-1]:
                            rsat.addConstraint(dict(zip([var],[-1 if coef < cc[1] else 1])), 0 if coef < cc[1] else 1)
                            if args.mistype > 0:
                                rsat_mis.addConstraint(dict(zip([var],[-1 if coef < cc[1] else 1])), 0 if coef < cc[1] else 1)
                            if coef < cc[1]:
                                time_print("Falsifying variable {}, from unit core {}".format(var, cc))
                            else:
                                time_print("Hardening variable {}, from unit core {}".format(var, cc))
                            hardened_vars[var-1] = True
                if len(c) == 0:
                    should_extract_abs_core = False
                    oldUB = UB
                    model_cost(m)
                    UB_tightened = oldUB != UB

            if len(hs1) <= 0:
                should_extract_generic_core = False
            #Extract standard generic core
            if should_extract_generic_core:
                core_start = time()
                c1, m1, s1 = get_core(rsat, list(hs1), shuffles=args.genericshuffles, genericCore=True, usedcores=args.usedgenericcores, knapsack=(not args.disableknapsack), rc=(rc if args.coreselectbyrc else {}))
                core_time = time() - core_start
                generic = []
                for cc in c1:
                    rhsshouldbe = 1
                    shouldBreak = False
                    for var,coef in cc[0].items():
                        if not ((obj_var_coef[abs(var)] > 0 and coef == 1) or (obj_var_coef[abs(var)] < 0 and coef == -1)):
                            generic.append(True)
                            shouldBreak = True
                            break
                        if coef == -1:
                            rhsshouldbe -= 1
                    if not shouldBreak:
                        generic.append(cc[1] != rhsshouldbe)
                genericcores += len([cc for cc in generic if cc])
                allcores1 += len(generic)
                time_print("Core size {}, coeffs {}, rhs {}, generic? {}, took {} seconds".format(list(map(lambda c: len(c[0]), c1)), list(map(lambda c: len(set(c[0].values())), c1)), list(map(lambda c: c[1], c1)), generic, core_time))
                total_core_time += core_time
                for cc in c1:
                    if len(cc[0]) == 1 and not args.nounitcorefix:
                        var,coef = list(cc[0].items())[0]
                        if not hardened_vars[var-1]:
                            rsat.addConstraint(dict(zip([var],[-1 if coef < cc[1] else 1])), 0 if coef < cc[1] else 1)
                            if args.mistype > 0:
                                rsat_mis.addConstraint(dict(zip([var],[-1 if coef < cc[1] else 1])), 0 if coef < cc[1] else 1)
                            if coef < cc[1]:
                                time_print("Falsifying variable {}, from unit core {}".format(var, cc))
                            else:
                                time_print("Hardening variable {}, from unit core {}".format(var, cc))
                            hardened_vars[var-1] = True
                if len(c1) == 0:
                    should_extract_generic_core = False
                    oldUB = UB
                    model_cost(m1)
                    UB_tightened = oldUB != UB

            if len(hs2) <= 0:
                should_extract_nongnrc_core = False
            #Extract standard non generic core
            if should_extract_nongnrc_core:
                core_start = time()
                c2, m2, s2 = get_core(rsat, list(hs2), shuffles=args.nongenericshuffles, genericCore=False, usedcores=args.usednongenericcores, rc=(rc if args.coreselectbyrc else {}))
                core_time = time() - core_start
                allcores2 += len(c2)
                time_print("{}Core size{} {}, took {} seconds".format("Standard " if args.abs > 0 else "", "s" if args.usednongenericcores > 1 else "", [len(cc) for cc in c2], core_time))
                total_core_time += core_time
                for cc in c2:
                    if len(cc[0]) == 1 and not args.nounitcorefix:
                        var,coef = list(cc[0].items())[0]
                        if not hardened_vars[var-1]:
                            rsat.addConstraint(dict(zip([var],[-1 if coef < cc[1] else 1])), 0 if coef < cc[1] else 1)
                            if args.mistype > 0:
                                rsat_mis.addConstraint(dict(zip([var],[-1 if coef < cc[1] else 1])), 0 if coef < cc[1] else 1)
                            if coef < cc[1]:
                                time_print("Falsifying variable {}, from unit core {}".format(var, cc))
                            else:
                                time_print("Hardening variable {}, from unit core {}".format(var, cc))
                            hardened_vars[var-1] = True
                if len(c2) == 0:
                    should_extract_nongnrc_core = False
                    oldUB = UB
                    model_cost(m2)
                    UB_tightened = oldUB != UB

            #No cores found, terminate disjoint phase
            if len(c) == 0 and len(c1) == 0 and len(c2) == 0:
                if wce_by_rc and disjoints == 0:
                    wce_by_rc = False
                    should_extract_abs_core = args.abs > 0
                    should_extract_generic_core = args.coretype in [1,2]
                    should_extract_nongnrc_core = args.coretype in [0,2]
                    assumps = set([h for h in hs])
                    hs1 = set([h for h in hs])
                    hs2 = set([h for h in hs])
                    time_print("Couldn't extract cores by rc, switching to objective based wce")
                    iterations_not_wcerc += 1
                    continue
                else:
                    break
            opt_hs = False

            #Minimize core(s)
            if args.mincore > 0:
                min_c = []
                for cc in c:
                    minimize_start = time()
                    mincore = minimize_core(rsat, list(cc[0].keys()), args.mincore)
                    mincore = [{abs(x):(1 if x > 0 else -1) for x in mincore}, 1 - sum([1 for x in mincore if x < 0])]
                    min_c.append(mincore)
                    minimize_time = time() - minimize_start
                    time_print("Minimizing Abstract core took {} seconds".format(minimize_time))
                    total_minimize_time += minimize_time
                c = min_c[:]
                min_c2 = []
                for cc in c2:
                    minimize_start = time()
                    mincore = minimize_core(rsat, list(cc[0].keys()), args.mincore)
                    mincore = [{abs(x):(1 if x > 0 else -1) for x in mincore}, 1 - sum([1 for x in mincore if x < 0])]
                    min_c2.append(mincore)
                    minimize_time = time() - minimize_start
                    time_print("Minimizing Standard core took {} seconds".format(minimize_time))
                    total_minimize_time += minimize_time
                c2 = min_c2[:]

            if args.abs > 0:
                time_print("Abstract core - Original vars: {}, abstract core vars: {}".format([sum(l < origMaxVar for l in cc[0].keys()) for cc in c], [sum(l >= origMaxVar for l in cc[0].keys()) for cc in c]))
            #Subtract literals from assumption set (not abstract cores)
            if len(c1) > 0:
                for cc in c1:
                    if args.wce == 0:
                        hs1.difference_update(set([-l for l in cc[0].keys()] + [l for l in cc[0].keys()]))
                    else:
                        min_core_coef = min([tmp_rc1[abs(l)] if wce_by_rc else tmp_obj_coef1[abs(l)] for l in cc[0].keys()])
                        for l in cc[0].keys():
                            if wce_by_rc:
                                tmp_rc1[abs(l)] -= min_core_coef
                            else:
                                tmp_obj_coef1[abs(l)] -= min_core_coef
                        hs1.difference_update(set([-l for l in cc[0].keys() if (wce_by_rc and tmp_rc1[abs(l)] <= 0) or (not wce_by_rc and tmp_obj_coef1[abs(l)] <= 0)] + [l for l in cc[0].keys() if (wce_by_rc and tmp_rc1[abs(l)] <= 0) or (not wce_by_rc and tmp_obj_coef1[abs(l)] <= 0)]))
            if len(c2) > 0:
                for cc in c2:
                    if args.wce == 0:
                        hs2.difference_update(set([-l for l in cc[0].keys()] + [l for l in cc[0].keys()]))
                    else:
                        min_core_coef = min([tmp_rc2[abs(l)] if wce_by_rc else tmp_obj_coef2[abs(l)] for l in cc[0].keys()])
                        for l in cc[0].keys():
                            if wce_by_rc:
                                tmp_rc2[abs(l)] -= min_core_coef
                            else:
                                tmp_obj_coef2[abs(l)] -= min_core_coef
                        hs2.difference_update(set([-l for l in cc[0].keys() if (wce_by_rc and tmp_rc2[abs(l)] <= 0) or (not wce_by_rc and tmp_obj_coef2[abs(l)] <= 0)] + [l for l in cc[0].keys() if (wce_by_rc and tmp_rc2[abs(l)] <= 0) or (not wce_by_rc and tmp_obj_coef2[abs(l)] <= 0)]))

            #Maintain variable graph based on found abstract core
            if args.abs > 0:
                abs_start = time()
                for cc in c:
                    for i1, l1 in enumerate(cc[0].keys()):
                        for i2, l2 in enumerate(cc[0].keys()):
                            if i1 >= i2 or l1 not in obj_var_coef or l2 not in obj_var_coef or obj_var_coef[l1] != obj_var_coef[l2]:
                                continue
                            if abs_core_graph.has_edge(abs(l1), abs(l2)):
                                weight = abs_core_graph[abs(l1)][abs(l2)]['weight']
                            else:
                                weight = 0
                            abs_core_graph.add_edge(abs(l1), abs(l2), weight=weight+1)
                for cc in c2:
                    for i1, l1 in enumerate(cc[0].keys()):
                        for i2, l2 in enumerate(cc[0].keys()):
                            if i1 >= i2 or l1 not in obj_var_coef or l2 not in obj_var_coef or obj_var_coef[l1] != obj_var_coef[l2]:
                                continue
                            if abs_core_graph.has_edge(abs(l1), abs(l2)):
                                weight = abs_core_graph[abs(l1)][abs(l2)]['weight']
                            else:
                                weight = 0
                            abs_core_graph.add_edge(abs(l1), abs(l2), weight=weight+1)
                total_abs_time += time() - abs_start

            #Encode cores to MCHS solver
            if len(c) > 0:
                for cc in c:
                    ihs.addConstraint(cc[0], 0, cc[1])
            if len(c1) > 0:
                for cc in c1:
                    ihs.addConstraint(cc[0], 0, cc[1])
            if len(c2) > 0:
                for cc in c2:
                    ihs.addConstraint(cc[0], 0, cc[1])
            time_print("{} linear constraints".format(ihs.getConstraintNumber()))
            #Subtract literals from assumption set (abstract)
            if len(c) > 0 and args.abs > 0:
                for cc in c:
                    min_core_coef = min([tmp_rc_abs[abs(l)] if wce_by_rc else tmp_obj_coef_abs[abs(l)] for l in cc[0].keys()])
                    for l in cc[0].keys():
                        if args.wce > 0:
                            if wce_by_rc:
                                tmp_rc_abs[abs(l)] -= min_core_coef
                            else:
                                tmp_obj_coef_abs[abs(l)] -= min_core_coef
                        if args.wce == 0 or (wce_by_rc and tmp_rc_abs[abs(l)] <= 0) or (not wce_by_rc and tmp_obj_coef_abs[abs(l)] <= 0):
                            if -l in assumps:
                                assumps.remove(-l)
                            if l in assumps:
                                assumps.remove(l)
            disjoints += len(c) + len(c1) + len(c2)

        time_print(disjoints, "disjoint cores")
        if args.abs > 0:
            time_print("{} abstract cores".format(len(abs_cores)))

        if opt_hs and args.optimalhs == 2: forceOptimalLB = True
        #UB == LB, terminate
        if (abs(UB - LB) < 1e-7): break

        if args.mistype in [2,3] and prevUB != UB:
            time_print("UB decreased, resetting MIS counter")
            MIS_counter = 0
        MIS_counter += 1
        if MIS_counter > args.misiters:
            rsat_mis.addConstraint({var:-coef for var, coef in obj_var_coef.items()}, -(UB - 1))
            mis_start = time()
            c, m, s = get_core(rsat_mis, [], shuffles=0, timeout=args.mistimeout)
            mis_time = time() - mis_start
            total_mis_time += mis_time
            time_print("Model improving search took {} seconds".format(mis_time))
            if s == "UNSAT":
                time_print("No better model exists")
                LB = UB
                break
            elif s == "SAT":
                time_print("Found better model")
                model_cost(m)
            MIS_counter = 0

    if args.wce != 2:
        print("c {} iterations".format(i))
    else:
        print("c {}/{} iterations with rc wce".format((i - iterations_not_wcerc), i))
    print("s", "OPTIMUM FOUND")
    print("o", int(LB) - coeff_sum)
    print("c Fixed variables: {}".format(len([f for f in hardened_vars if f])))
    print("c Amount of original variables: {} - overall variables {}".format(origMaxVar, nextVar))
    if args.coretype in [1,2]:
        print("c generic cores out of cores in generic disjoint phase: {}/{}".format(genericcores, allcores1))
    if args.coretype in [0,2]:
        print("c cores in non-generic disjoint phase: {}".format(allcores2))
    if args.printsolution:
        print("c best model:")
        print("v ", end='')
        for mm in best_model:
            print("{} ".format(mm), end='')
    print("c")

    log_exit(rsat)
