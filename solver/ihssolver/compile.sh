#Replace following directories with location of the CPLEX library in your system
CPLEXDIR=/opt/ibm/ILOG/CPLEX_Studio128/cplex
CPLEXLIBDIR=/opt/ibm/ILOG/CPLEX_Studio128/cplex/lib/x86-64_linux/static_pic
CONCERTDIR=/opt/ibm/ILOG/CPLEX_Studio128/concert
CONCERTLIBDIR=/opt/ibm/ILOG/CPLEX_Studio128/concert/lib/x86-64_linux/static_pic

IP_LNDIRS="-L${CPLEXLIBDIR} -L${CONCERTLIBDIR}"
IP_LNFLAGS="-lconcert -lilocplex -lcplex -lm -lpthread -ldl"
IP_INCLUDES="-DIL_STD -I${CPLEXDIR}/include -I${CONCERTDIR}/include"

WFLAGS="-Wall -Wextra -pedantic -Wno-ignored-attributes"

g++ -O3 ${WFLAGS} -shared --std=c++14 -fPIC ${IP_INCLUDES} ihssolver.cpp ${IP_LNDIRS} ${IP_LNFLAGS} $(python3 -m pybind11 --includes) -o ihssolver$(python3-config --extension-suffix)
