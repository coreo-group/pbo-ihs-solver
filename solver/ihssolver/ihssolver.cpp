#include <ilcplex/ilocplex.h>
#include <pybind11/pybind11.h>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <exception>

namespace py = pybind11;

bool is_optimal_solution;
ILOMIPINFOCALLBACK2(LBCutoffCallback,
                    IloNum, lb, IloNum, ub)
{
    if ( hasIncumbent() ) {
        double incumbentValue = getIncumbentObjValue();
        if (incumbentValue <= lb) {
            std::cout << "c IP LB cutoff " << incumbentValue << std::endl;
            abort();
        } else if (incumbentValue < ub) {
            std::cout << "c early abort: " << incumbentValue << " below UB" << std::endl;
            is_optimal_solution = false;
            abort();
        }
    }
}

class Solver
{
    public:
    Solver(int nvars)
    {
        model = IloModel(env);
        model_lp = IloModel(env);
        varArray = IloNumVarArray(env);
        varArrayLp = IloNumVarArray(env);
        for (int i = 0; i < nvars; i++)
        {
            char name[11];
            snprintf(name, 11, "%10d", i + 1);
            IloNumVar xx = IloNumVar(env, 0, 1, ILOBOOL, name);
            IloNumVar xx_lp = IloNumVar(env, 0, 1, ILOFLOAT, name);
            x.push_back(xx);
            x_lp.push_back(xx_lp);
            coeffs.push_back(0);
        }
        cplex = IloCplex(model);
        cplex.setParam(IloCplex::Threads, 1);
        cplex.setParam(IloCplex::Param::RandomSeed, 123123);
        cplex.setParam(IloCplex::EpGap, 0.0);
        cplex.setParam(IloCplex::EpAGap, 0.0);
        cplex.setParam(IloCplex::MIPDisplay, 0);
        cplex.setOut(env.getNullStream());

        cplex_lp = IloCplex(model_lp);
        cplex_lp.setParam(IloCplex::Threads, 1);
        cplex_lp.setParam(IloCplex::Param::RandomSeed, 123123);
        cplex_lp.setParam(IloCplex::EpGap, 0.0);
        cplex_lp.setParam(IloCplex::EpAGap, 0.0);
        cplex_lp.setParam(IloCplex::MIPDisplay, 0);
        cplex_lp.setOut(env.getNullStream());
    }

    ~Solver()
    {
        env.end();
    }

    void addObjective(std::string objective)
    {
        try {
            std::istringstream iss(objective);
            std::string wrd;
            IloExpr expr(env);
            IloExpr expr_lp(env);
            while ((iss >> wrd))
            {
                if (wrd[0] == '+' || wrd[0] == '-')
                {
                    std::string strVar;
                    iss >> strVar;
                    int var = std::stoi(strVar.substr(1));
                    long coeff;
                    if (wrd[0] == '+')
                        coeff = std::stol(wrd.substr(1));
                    else
                        coeff = std::stol(wrd);
                    expr += coeff * x[var - 1];
                    varArray.add(x[var - 1]);
                    expr_lp += coeff * x_lp[var - 1];
                    varArrayLp.add(x_lp[var - 1]);
                    coeffs[var - 1] = coeff;
                }
            }
            model.add(IloMinimize(env, expr));
            model_lp.add(IloMinimize(env, expr_lp));
        }
        catch (IloException& e) {
            std::cerr << "Concert exception caught in addObjective: " << e << std::endl;
        }
        catch (std::exception& e) {
            std::cerr << "Unknown exception caught in addObjective: " << e.what() << std::endl;
        }
    }

    void addConstraint(std::string constraint)
    {
        try {
            std::istringstream iss(constraint);
            std::string wrd;
            IloExpr expr(env);
            IloExpr expr_lp(env);
            while ((iss >> wrd))
            {
                if (wrd == ">=")
                {
                    long rhs;
                    iss >> rhs;
                    model.add(expr >= rhs);
                    model_lp.add(expr_lp >= rhs);
                    break;
                }
                if (wrd == "=")
                {
                    long rhs;
                    iss >> rhs;
                    model.add(expr == rhs);
                    model_lp.add(expr_lp == rhs);
                    break;
                }
                if (wrd == "<=")
                {
                    long rhs;
                    iss >> rhs;
                    model.add(expr <= rhs);
                    model_lp.add(expr_lp <= rhs);
                    break;
                }
                if (wrd[0] == '+' || wrd[0] == '-')
                {
                    std::string strVar;
                    iss >> strVar;
                    int var = std::stoi(strVar.substr(1));
                    long coeff;
                    if (wrd[0] == '+')
                        coeff = std::stol(wrd.substr(1));
                    else
                        coeff = std::stol(wrd);
                    expr += coeff * x[var - 1];
                    expr_lp += coeff * x_lp[var - 1];
                }
            }
            ++constraintNumber;
        }
        catch (IloException& e) {
            std::cerr << "Concert exception caught in addConstraint(string): " << e << std::endl;
        }
        catch (std::exception& e) {
            std::cerr << "Unknown exception caught in addConstraint(string): " << e.what() << std::endl;
        }
    }

    void addConstraint(py::dict expression, int sense, long rhs)
    {
        try {
            IloExpr expr(env);
            IloExpr expr_lp(env);
            for (auto pair : expression) {
                expr += pair.second.cast<long>() * x[pair.first.cast<int>() - 1];
                expr_lp += pair.second.cast<long>() * x_lp[pair.first.cast<int>() - 1];
            }

            switch (sense)
            {
            case 0:
                model.add(expr >= rhs);
                model_lp.add(expr_lp >= rhs);
                break;
            case 1:
                model.add(expr <= rhs);
                model_lp.add(expr_lp <= rhs);
                break;
            case 2:
                model.add(expr == rhs);
                model_lp.add(expr_lp == rhs);
                break;
            default:
                std::cout << "Sense of value " << sense << " not supported" << std::endl;
                break;
            }
            ++constraintNumber;
        }
        catch (IloException& e) {
            std::cerr << "Concert exception caught in addConstraint(dict, int, long): " << e << std::endl;
        }
        catch (std::exception& e) {
            std::cerr << "Unknown exception caught in addConstraint(dict, int, long): " << e.what() << std::endl;
        }
    }

    void addVariable(int varId)
    {
        char name[15];
        snprintf(name, 15, "abs_%10d", varId);
        IloNumVar xx = IloNumVar(env, 0, 1, ILOBOOL, name);
        x.push_back(xx);
        IloNumVar xx_lp = IloNumVar(env, 0, 1, ILOFLOAT, name);
        x_lp.push_back(xx_lp);
    }

    int getConstraintNumber()
    {
        return constraintNumber;
    }

    py::tuple solve(long LB, long UB)
    {
        is_optimal_solution = true;
        long long actualSolution = 0;
        py::list hs;
        try {
            cplex.use(LBCutoffCallback(env, LB, UB));
            cplex.solve();
            //CPLEX in PBO-IHS should never return infeasible, reconstruct CPLEX instance and solve it again
            while (cplex.getStatus() != IloAlgorithm::Optimal && cplex.getStatus() != IloAlgorithm::Feasible) {
                std::cout << "IP solver returned " << cplex.getStatus() << " - reconstructing IP instance" << std::endl;
                cplex = IloCplex(model);
                cplex.setParam(IloCplex::Threads, 1);
                cplex.setParam(IloCplex::Param::RandomSeed, 123123);
                cplex.setParam(IloCplex::EpGap, 0.0);
                cplex.setParam(IloCplex::EpAGap, 0.0);
                cplex.setParam(IloCplex::MIPDisplay, 0);
                //cplex.setParam(IloCplex::Param::Simplex::Limits::Singularity, 100);
                cplex.setOut(env.getNullStream());
                cplex.solve();
            }

            IloNumArray vals(env);
            cplex.getValues(vals, varArray);
            //cplex.out() << "Solution status " << cplex.getStatus() << std::endl;
            //cplex.out() << "o " << (long)cplex.getObjValue() << std::endl;
            //cplex.out() << "v";
            for (int i = 0; i < vals.getSize(); ++i)
            {
                int idx = std::stoi(varArray[i].getName());
                if (vals[i] > 0.5)
                {
                    //cplex.out() << " x" << (i+1);
                    actualSolution += coeffs[idx - 1];
                    hs.append(idx);
                }
                else
                {
                    //cplex.out() << " -x" << (i+1);
                    hs.append(-idx);
                }
            }
            cplex.setParam(IloCplex::RootAlg, IloCplex::Dual);
            //cplex.out() << std::endl << "actual solution: " << actualSolution << std::endl;
        }
        catch (IloException& e) {
            std::cerr << "Concert exception caught in solve(): " << e << std::endl;
        }
        catch (std::exception& e) {
            std::cerr << "Unknown exception caught in solve(): " << e.what() << std::endl;
        }

        return py::make_tuple(actualSolution, hs, is_optimal_solution);
    }

    py::tuple solve_lp_relaxation()
    {
        /* Unlike other solvers lp_relaxation returns the lp optimal value
            of all cplex variables---not just the bvars */

        py::dict vals_py;
        py::dict reds_py;
        double solution = 0;
        try {
            cplex_lp.solve();
            //CPLEX in PBO-IHS should never return infeasible, reconstruct CPLEX instance and solve it again
            while (cplex_lp.getStatus() != IloAlgorithm::Optimal) {
                std::cout << "LP solver returned " << cplex_lp.getStatus() << " - reconstructing LP instance" << std::endl;
                cplex_lp = IloCplex(model_lp);
                cplex_lp.setParam(IloCplex::Threads, 1);
                cplex_lp.setParam(IloCplex::Param::RandomSeed, 123123);
                cplex_lp.setParam(IloCplex::EpGap, 0.0);
                cplex_lp.setParam(IloCplex::EpAGap, 0.0);
                cplex_lp.setParam(IloCplex::MIPDisplay, 0);
                //cplex_lp.setParam(IloCplex::Param::Simplex::Limits::Singularity, 100);
                cplex_lp.setOut(env.getNullStream());
                cplex_lp.solve();
            }

            IloNumArray vals(env);
            IloNumArray reducedCosts(env);
            cplex_lp.getValues(vals, varArrayLp);
            cplex_lp.getReducedCosts(reducedCosts, varArrayLp);
            //cplex.out() << "Solution status " << cplex.getStatus() << std::endl;
            //cplex.out() << "o " << (long)cplex.getObjValue() << std::endl;
            //cplex.out() << "v";
            for (int i = 0; i < vals.getSize(); ++i)
            {
                py::int_ key = py::int_(std::stoi(varArrayLp[i].getName()));
                vals_py[key] = vals[i];
                reds_py[key] = reducedCosts[i];
            }
            solution = cplex_lp.getObjValue();
            cplex_lp.setParam(IloCplex::RootAlg, IloCplex::Dual);
            //cplex.out() << std::endl << "actual solution: " << actualSolution << std::endl;
        }
        catch (IloException& e) {
            std::cerr << "Concert exception caught in solve_lp_relaxation(): " << e << std::endl;
        }
        catch (std::exception& e) {
            std::cerr << "Unknown exception caught in solve_lp_relaxation(): " << e.what() << std::endl;
        }

        return py::make_tuple(solution, vals_py, reds_py);
    }

    void resetSolvers() {
        cplex_lp = IloCplex(model_lp);
        cplex_lp.setParam(IloCplex::Threads, 1);
        cplex_lp.setParam(IloCplex::Param::RandomSeed, 123123);
        cplex_lp.setParam(IloCplex::EpGap, 0.0);
        cplex_lp.setParam(IloCplex::EpAGap, 0.0);
        cplex_lp.setParam(IloCplex::MIPDisplay, 0);
        cplex_lp.setOut(env.getNullStream());

        cplex = IloCplex(model);
        cplex.setParam(IloCplex::Threads, 1);
        cplex.setParam(IloCplex::Param::RandomSeed, 123123);
        cplex.setParam(IloCplex::EpGap, 0.0);
        cplex.setParam(IloCplex::EpAGap, 0.0);
        cplex.setParam(IloCplex::MIPDisplay, 0);
        cplex.setOut(env.getNullStream());
    }
    void exportModels(std::string modelName) {
        cplex_lp.exportModel((modelName + "_lp.lp").c_str());
        cplex.exportModel((modelName + "_ip.lp").c_str());
    }

    IloEnv env;
    IloModel model;
    IloModel model_lp;
    IloNumVarArray varArray;
    IloNumVarArray varArrayLp;
    std::vector<IloNumVar> x;
    std::vector<IloNumVar> x_lp;
    std::vector<long> coeffs;
    int constraintNumber = 0;
    IloCplex cplex;
    IloCplex cplex_lp;
};

PYBIND11_MODULE(ihssolver, m) {
    py::class_<Solver>(m, "Solver")
        .def(py::init<int>())
        .def("addObjective", &Solver::addObjective)
        .def("addConstraint", (void (Solver::*)(std::string)) &Solver::addConstraint)
        .def("addConstraint", (void (Solver::*)(py::dict, int, long)) &Solver::addConstraint)
        .def("addVariable", &Solver::addVariable)
        .def("getConstraintNumber", &Solver::getConstraintNumber)
        .def("solve_lp_relaxation", &Solver::solve_lp_relaxation)
        .def("solve", &Solver::solve)
        .def("resetSolvers", &Solver::resetSolvers)
        .def("exportModels", &Solver::exportModels);
}
